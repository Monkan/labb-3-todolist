$(document).ready(function () {
var textfield = $("input");
var text = textfield.val();
var addButton = $("#add");
var deleteButton = $("#delete");

var ItemList = function(element) {
  this.element = element;
  this.list = [];
  //this.nextId = 0;
};

ItemList.prototype.render = function() {
  for(var i = 0; i < this.list.length; i++) {
    this.list[i].render();
  }
};

ItemList.prototype.add = function(text) {
  var item = new Item(text, this);
  this.list.push(item);
  this.element.append(item.element);
  //this.element.on('click', this.onClick.bind(this));
};

/*ItemList.prototype.add = function(text) {
  var item = new Item(this.nextId, text, this);
  this.nextId++;
  this.list.push(item);
  this.element.append(item.element);
};*/

ItemList.prototype.remove = function() {
  for (var i = this.list.length-1; i >= 0; i--) {
      var item = this.list[i];
      if (item.isMarked) {
        this.list.splice(i, 1);
        item.element.remove();
    }
  }
};

/*ItemList.prototype.remove = function(id) {
  for(var i in this.list) {
    if(this.list[i].id === id) {
      this.list.splice(i, 1);
    }
  }
};*/

var Item = function(text, list){
  this.list = list;
  //this.id = id;
  this.text= text;
  this.isMarked = false;
  this.element = $("<li></li>");
  this.element.on("click", this.onClick.bind(this));
};

/*var Item = function(id, text, list){
  this.list = list;
  this.id = id;
  this.text= text;
  this.element = $("<li></li>");
  this.element.on("click", this.onClick.bind(this));
};*/

Item.prototype.render = function () {
  this.element.html(this.text);
  (this.isMarked) ? this.element.addClass('marked') : this.element.removeClass('marked');
};

Item.prototype.onClick = function() {
  //this.element.toggleClass('active');
  this.isMarked = !this.isMarked;
  this.render();
};

$("#add").on("click", function(){
  var itemList = new ItemList($("ul"));
  var text = $("input").val();
  itemList.add(text);
  itemList.render();
  textfield.val(" ");
});

$("#delete").on("click", function(){
  itemList.remove();
});

});